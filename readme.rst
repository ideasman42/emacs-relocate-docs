
#############
Relocate Docs
#############

Utility to relocate doc-strings into headers.

Example use:

.. code-block:: sh

   relocate_docs.el --build-dir=/path/to/build $(find source/blender/bmesh/ -iname "*.c*")


When operating on many files (100's or 1000's), ``clangd`` may run out of memory.
In this case files can be operated on one at a time.

.. code-block:: sh

   for f in $(find path/to/source -iname "*.c*"); do relocate_docs.el --build-dir=/path/to/build $f; done

Usage
=====

Command line arguments:

``--build-dir`` (required)
   Path to the build directory.
``--exclude-regexp``
   Regular expression to exclude when moving comments.

   This example excludes comments containing doxygen groups ``\{`` and ``\}``.

   .. code-block:: sh

      relocate_docs.el --build-dir=/path/to/build $(find src/ -iname "*.c*") --exclude-regexp="\(\\\{\|\\\}\)" $f; done


Details
=======

- C/C++ source files need to be given as arguments,
  doc-strings from public symbols are then moved into headers.
- Doc-strings are copied when they are directly above the symbol (with no blank lines).
- Doc-strings are only ever moved between files in the project.


Dependencies
============

- ``emacs``.
- ``clangd``.
