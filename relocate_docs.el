#!/usr/bin/env -S emacs --script
;;; relocate_docs.el --- Relocate doc-strings from sources into headers -*- lexical-binding: t -*-

;; Copyright (C) 2021  Campbell Barton

;; Author: Campbell Barton <ideasman42@gmail.com>

;; URL: https://gitlab.com/ideasman42/emacs-relocate-docs
;; Version: 0.1
;; Package-Requires: ((emacs "27.0"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Summary:

;; Utility to migrate doc-strings from sourcre into headers.

;;; Commentary:

;; NOTE(@campbellbarton): this could use some re-working if the project
;; was going to be used more generally.
;; As it stands, this is for a once off migration of doc-strings.

;;; Usage:

;; Requires `clangd' to be installed on the system.
;;
;; Example command line execution:
;;
;;    relocate_docs.el --build-dir=/path/to/build_dir source/blender/sequencer/intern/*.c*
;;
;; Or using the find command:
;;
;;    relocate_docs.el --build-dir=/path/to/build_dir $(find source/blender/python -iname "*.c*")
;;

;;; Code:

;; For scanning the buffer for symbols.
(require 'imenu)
;; For looking up symbols.
(require 'xref)

(defvar relocate_docs-prompt nil
  "Prompt for user input before applying an edit, otherwise apply all edits.")

(defvar relocate_docs-prompt-editor nil
  "The editor to launch when previewing the location for moving documentation.")

(defvar relocate_docs-build-dir nil
  "The directory to use for CLANGD (must contain 'compile_commands.json').")

(defvar relocate_docs-delimit-blank-lines t
  "Don't allow empty lines between the doc-string and the code.")

(defvar relocate_docs-exclude-regexp nil
  "Optionally exclude comments matching this regular-expression")

(defvar relocate_docs-jobs
  (cond
   ((version< emacs-version "29.0")
    8)
   (t
    (num-processors)))
  "Number of jobs to use.")

;; ---------------------------------------------------------------------------
;; Generic Utilities

(defun relocate_docs-current-line-empty-p ()
  (save-excursion
    (beginning-of-line)
    (looking-at-p "[[:blank:]]*$")))

(defun relocate_docs--regexp-error-check (string)
  "Return an error string if STRING is not a valid regexp, otherwise nil."
  (condition-case err
      (prog1 nil
        (string-match-p string ""))
    (error (or (error-message-string err) "unknown error"))))

(defun relocate_docs--error-and-exit (x &rest args)
  (apply #'message (cons x args))
  (kill-emacs 1))

(defmacro relocate_docs--with-advice (fn-orig where fn-advice &rest body)
  "Execute BODY with WHERE advice on FN-ORIG temporarily enabled."
  (declare (indent 1))
  `(let ((fn-advice-var ,fn-advice))
     (unwind-protect
         (progn
           (advice-add ,fn-orig ,where fn-advice-var)
           ,@body)
       (advice-remove ,fn-orig fn-advice-var))))

(defun relocate_docs--root-path-from-filename (filepath &optional default)
  "Get the project root from FILEPATH (or return DEFAULT)."
  (or (let ((vc-backend
             (ignore-errors
               (vc-responsible-backend filepath))))
        (when vc-backend
          (let ((vc-base-path (vc-call-backend vc-backend 'root filepath)))
            vc-base-path)))
      default))

(defun relocate_docs--root-path-from-current-buffer (&optional default)
  "Get the project root from the current buffer (or return DEFAULT)."
  (relocate_docs--root-path-from-filename (file-truename (buffer-file-name)) default))

(defun relocate_docs--extract-arg-impl (args id required fn)
  "See `relocate_docs--extract-arg-value' for docs on ARGS ID and REQUIRED.
FN is called on each item, a non-nil return value is returned."
  (let ((args-iter args)
        (args-prev nil)
        (result nil))

    (while args-iter
      (let ((item (car args-iter)))
        (let ((result-test (funcall fn item)))
          (when result-test
            (setq result result-test)
            (cond
             (args-prev
              (setcdr args-prev (cdr args-iter)))
             (t
              (setq args (cdr args-iter))))
            ;; Break.
            (setq args-iter nil))))

      (setq args-prev args-iter)
      (setq args-iter (cdr args-iter)))

    (when (and required (null result))
      (relocate_docs--error-and-exit "Argument %S not given!" id))
    (cons args result)))

(defmacro relocate_docs--extract-arg-value (args id &optional required)
  "Extract '--arg=foo' style argument from ARGS (removing it from the list).
ID is the argument to look for '--arg' in this case.
REQUIRED throws an error if the argument is not found.
Return the value (as a string) if the argument is found, otherwise nil."
  `(let* ((id-var ,id)
          (id-prefix (concat id-var "="))
          (result
           (relocate_docs--extract-arg-impl
            ,args id-var ,required
            (lambda (item)
              (cond
               ((string-equal (substring item 0 (length id-prefix)) id-prefix)
                (substring item (length id-prefix) (length item)))
               (t
                nil))))))

     (setq ,args (car result))
     (cdr result)))

(defmacro relocate_docs--extract-arg-exist (args id &optional required)
  "Extract '--arg' style argument from ARGS (removing it from the list).
ID is the argument to look for '--arg' in this case.
REQUIRED throws an error if the argument is not found.
Return t if the argument is found."
  `(let* ((id-var ,id)
          (result
           (relocate_docs--extract-arg-impl
            ,args id-var ,required (lambda (item) (string-equal item id-var)))))

     (setq ,args (car result))
     (cdr result)))


;; ---------------------------------------------------------------------------
;; Package Management

(defvar relocate_docs--ensure-package-has-refresh nil)

(defun relocate_docs--package-init ()
  "Setup the package manager defaults."
  (with-demoted-errors "Relocate docs error: %S"
    (require 'package)
    (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
    (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
    (package-initialize)))

(defun relocate_docs--package-ensure (pkg)
  "Ensure PKG is installed."
  (unless (package-installed-p pkg)
    (unless relocate_docs--ensure-package-has-refresh
      (setq relocate_docs--ensure-package-has-refresh t)
      (package-refresh-contents))
    (message "Package %S not found, installing.." pkg)
    (package-install pkg)))


;; ---------------------------------------------------------------------------
;; Logging Functions

(defun relocate_docs--log-type-impl (prefix str)
  "Logging function (implementation), PREFIX and STR (with no newline)."
  (let ((buf (get-buffer-create "*relocate_docs-log*")))
    (set-text-properties 0 (length str) nil str)
    (princ (concat prefix str "\n") #'external-debugging-output)
    (with-current-buffer buf
      (insert prefix str "\n"))))

(defmacro relocate_docs--log-fail (&rest args)
  "Log failure messages formatted with ARGS."
  `(relocate_docs--log-type-impl "Fail: " (format ,@args)))

(defmacro relocate_docs--log-info (&rest args)
  "Log info messages formatted with ARGS."
  `(relocate_docs--log-type-impl "Info: " (format ,@args)))


;; ---------------------------------------------------------------------------
;; Logging Functions

(defun relocate_docs-filter-for-cc-mode (sym)
  "Return non-nil when this SYM should be checked for a doc-string.

The point will be located over the symbol (typically at it's beginning),
the point should not be moved by this function."
  (let ((prefix (buffer-substring-no-properties (line-beginning-position) (point))))
    (cond
     ;; Forward declaring structs shouldn't show documentation, e.g:
     ;;    struct SomeStruct;
     ;; while this is in some sense a personal preference,
     ;; forward declarations are mostly used to prevent warnings when these
     ;; structs are used as parameters.
     ;; So it makes sense to ignore them.
     ( ;; Match: struct sym;
      (and (string-match-p "^\\_<struct\\_>" prefix)
           (equal ?\; (char-after (+ (point) (length sym)))))
      nil)
     ;; Including `typedef' rarely gains anything from in-lining doc-string
     ;; Similar to `struct':
     ;; - This is already the declaration so the doc-string is already available.
     ;; - This forward declares an opaque type.
     ((string-match-p "^\\_<typedef\\_>" prefix)
      nil)
     (t
      t))))

;; See https://emacs.stackexchange.com/questions/30673
;; (adapted from `which-function').
(defun relocate_docs--positions-from-buffer ()
  "Return `imenu' positions for the current buffer between POS-BEG and POS-END."
  ;; Ensure `imenu--index-alist' is populated.

  ;; Note that in some cases a file will fail to parse,
  ;; typically when the file is intended for another platform (for example).
  (condition-case-unless-debug err
      (imenu--make-index-alist)
    (error (relocate_docs--log-fail "failed to use IMENU to access symbols: %s" err)))

  (let ((alist imenu--index-alist)
        (pair nil)
        (mark nil)
        (imstack nil)
        (result nil))
    ;; Elements of alist are either ("name" . marker), or
    ;; ("submenu" ("name" . marker) ... ). The list can be
    ;; Arbitrarily nested.
    (while (or alist imstack)
      (cond
       (alist
        (setq pair (car-safe alist))
        (setq alist (cdr-safe alist))
        (cond
         ((atom pair)) ;; Skip anything not a cons.

         ((imenu--subalist-p pair)
          (setq imstack (cons alist imstack))
          (setq alist (cdr pair)))

         ((number-or-marker-p (setq mark (cdr pair)))
          (let ((pos (marker-position mark)))
            (push pos result)))))
       (t
        (setq alist (car imstack))
        (setq imstack (cdr imstack)))))

    result))

(defun relocate_docs--c-generic-mode ()
  "Mode hook for C/C++ buffers."

  ;; For lookups (via xref).
  (require 'lsp)


  ;; Add project root to the session,
  (let ((project-root (relocate_docs--root-path-from-current-buffer)))
    (lsp-workspace-folders-add project-root))

  (lsp))

(with-eval-after-load 'lsp
  (setq-default lsp-response-timeout 1000)
  (setq-default lsp-log-io t)

  ;; Prefer CLANGD (others may be fine bot CLANGD is well tested).
  (setq-default lsp-disabled-clients '(ccls))
  (setq-default lsp-clients-clangd-args
                (list
                 (format "-j=%d" relocate_docs-jobs)
                 "--log=error"
                 (concat "--compile-commands-dir=" relocate_docs-build-dir)))
  (setq-default lsp-idle-delay 0.0)

  ;; Quiet "Connected to ..." messages (harmless but noisy!)
  (defun relocate_docs--disable-lsp-conn-msg-advice (func &rest r)
    (unless (string-prefix-p "Connected to " (car r))
      (apply func r)))
  (advice-add 'lsp--info :around #'relocate_docs--disable-lsp-conn-msg-advice)

  ;; Quiet warnings.
  (setq-default lsp-diagnostics-provider :none)
  (setq-default lsp-enable-snippet nil))

(defun relocate_docs--calc-comment-rage-before-point (pos)
  "Return comment before POS."
  (save-excursion
    (goto-char pos)
    (goto-char (line-beginning-position))
    (let ((pos-end-bol (point))
          (pos-end nil)
          ;; (pos-end-trim nil)
          (is-comment-ignored nil))

      (skip-chars-backward " \t" (point-min)) ;; '\n' if we want space between comments.
      (setq pos-end (point))

      (cond
       (relocate_docs-delimit-blank-lines
        (skip-chars-backward " \t" (point-min))
        (forward-char -1)
        (cond
         ((relocate_docs-current-line-empty-p)
          (setq is-comment-ignored t))
         (t
          (skip-chars-backward " \t" (point-min)))))
       (t
        ;; Allow blank lines between doc-strings and code.
        (skip-chars-backward " \t\n" (point-min))))

      ;; (setq pos-end-trim (point))
      (cond
       (is-comment-ignored
        nil)
       ((forward-comment -1)
        (let ((pos-beg (point))
              (pos-beg-of-line (line-beginning-position)))

          (cond
           ( ;; Ensure the comment is not a trailing comment of a previous line.
            (not
             (eq
              pos-beg-of-line
              (save-excursion
                (skip-chars-backward " \t" pos-beg-of-line)
                (point))))
            (relocate_docs--log-info
             "symbol in %S at point %d is previous lines trailing comment"
             (current-buffer)
             (point))
            ;; Skip this comment.
            nil)

           ;; Optionally exclude a regexp.
           ((and relocate_docs-exclude-regexp
                 (save-match-data
                   (goto-char pos-beg)
                   (search-forward-regexp relocate_docs-exclude-regexp pos-end t)))

            (relocate_docs--log-info
             "comment in %S at %d matches exclude expression %s, skipping"
             (current-buffer)
             pos-beg
             relocate_docs-exclude-regexp)

            ;; Skip this comment.
            nil)

           (t
            ;; Success.
            (cons pos-beg pos-end)))))
       (t
        nil)))))

(add-hook 'c-mode-hook 'relocate_docs--c-generic-mode)
(add-hook 'c++-mode-hook 'relocate_docs--c-generic-mode)


;; ---------------------------------------------------------------------------
;; Edit List App/Apply

(defun relocate_docs--edit-list-add (edit file-edit-map)
  "Add EDIT to FILE-EDIT-MAP."
  (pcase-let ((`(,text-range ,text ,buf) edit))
    (let ((buf-filename (buffer-file-name buf)))
      (let ((value (gethash buf-filename file-edit-map)))
        (unless value
          (setq value (cons buf (list)))
          (puthash buf-filename value file-edit-map))

        (let ((item (list (car text-range) (cdr text-range) text)))
          ;; Push item into the head of the cell `cdr'.
          (setcdr value (cons item (cdr value))))))))

(defun relocate_docs--edit-list-apply (file-edit-map)
  "Apply FILE-EDIT-MAP to buffers & save."
  (let ((edit-hash-as-list nil))
    (maphash (lambda (key value) (push (cons key value) edit-hash-as-list)) file-edit-map)
    ;; Sort by path name (not needed, displays a little nicer to perform edits in-order).
    (setq edit-hash-as-list (sort edit-hash-as-list (lambda (a b) (string-lessp (car a) (car b)))))
    (dolist (key-value-pair edit-hash-as-list)
      (pcase-let ((`(,key . ,value) key-value-pair))
        (pcase-let ((`(,buf . ,edits) value))
          ;; Largest edit to smallest (assume none overlap).
          (message "Edit: %s (%d changes)" key (length edits))
          ;; Sort edits is important, so they are performed bottom to top,
          ;; and points don't become invalid.
          (setq edits (sort edits (lambda (a b) (> (car a) (car b)))))
          (with-current-buffer buf
            (dolist (edit edits)
              (pcase-let ((`(,beg ,end ,text) edit))
                (unless (eq beg end)
                  (delete-region beg end))
                (goto-char beg)
                (insert text)))
            (save-buffer)))))))


;; ---------------------------------------------------------------------------
;; Main Doc-String Relocation Logic

(defun relocate_docs--xref-list-from-definitions (sym)
  "Return a list of XREF items from the identifier SYM at the current point.

Argument XREF-BACKEND is used to avoid multiple calls to `xref-find-backend'."
  (let ((xref-list nil))
    (relocate_docs--with-advice 'xref--not-found-error :override (lambda (_kind _input) nil)
      (relocate_docs--with-advice 'xref--show-defs :override
        (lambda (fetcher _display-action) (setq xref-list (funcall fetcher)))
        (let ((xref-prompt-for-identifier nil))
          ;; Needed to suppress `etags' from requesting a file.
          (with-demoted-errors "Relocate docs error: %S"
            (xref-find-definitions sym)))))
    xref-list))

(defun relocate_docs--doc-from-xref (text xref-list project-root)
  "Return an edit, inserting TEXT into one of the items in XREF-LIST.
Only files under PROJECT-ROOT are considered."
  ;; Build a list of comments from the `xref' list (which may find multiple sources).
  ;; In most cases only a single item is found.
  ;; Nevertheless, best combine all so a doc-string will be extracted from at least one.

  (let ((current-buf (current-buffer))
        (edit nil))
    ;; Don't enable additional features when loading files
    (save-excursion
      (relocate_docs--with-advice 'run-mode-hooks :override (lambda (_hooks) nil)

        (while xref-list
          (let* ((xref-item (pop xref-list))
                 ;; This sets '(point)' which is OK in this case.
                 (marker (xref-location-marker (xref-item-location xref-item)))
                 (buf (marker-buffer marker)))
            ;; Ignore matches in the same buffer.
            ;; While it's possible doc-strings could be at another location within this buffer,
            ;; in practice, this is almost never done.
            (cond
             ((eq buf current-buf)
              nil)
             ;; Never move comments into source files.
             ((member (file-name-extension (buffer-file-name buf)) (list ".c" ".cc" ".cpp"))
              nil)
             ;; Never move comments into files outside the project.
             ((not
               (string-equal
                project-root
                (relocate_docs--root-path-from-filename (file-truename (buffer-file-name buf))
                                                        "")))
              nil)
             (t
              (with-current-buffer buf
                (goto-char marker)
                (goto-char (line-beginning-position))

                ;; Break.
                (setq xref-list nil)

                (setq edit (list (cons (point) (point)) text buf)))))))))
    edit))

(defun relocate_docs--handle (project-root file-edit-map)
  "Move doc-strings out of the current buffer to other headers in PROJECT-ROOT."
  (let ((current-buf (current-buffer))
        (points (sort (relocate_docs--positions-from-buffer) '>))
        (edit-point nil)
        (xref-backend (xref-find-backend)))

    (dolist (pos points)
      (goto-char pos)
      (let ((sym (xref-backend-identifier-at-point xref-backend)))
        (when (and sym (relocate_docs-filter-for-cc-mode sym) (looking-at (regexp-quote sym)))
          (setq edit-point
                (save-excursion
                  (goto-char (match-beginning 0))
                  (line-beginning-position)))
          (when-let* ((text-range (relocate_docs--calc-comment-rage-before-point edit-point))
                      (text (buffer-substring-no-properties (car text-range) (cdr text-range)))
                      (xref-list (relocate_docs--xref-list-from-definitions sym))
                      (edit (relocate_docs--doc-from-xref text xref-list project-root)))

            (when (and relocate_docs-prompt relocate_docs-prompt-editor)
              ;; Open the file in an editor (for review...)
              (call-process "nohup"
                            nil 0 nil
                            ;; Command.
                            relocate_docs-prompt-editor
                            ;; File & line.
                            (concat
                             (file-truename (buffer-file-name current-buf))
                             ":"
                             (format "%d" (count-lines (point-min) pos)))))

            (when (or (not relocate_docs-prompt) (y-or-n-p "Apply edit?"))
              (relocate_docs--edit-list-add edit file-edit-map)
              (relocate_docs--edit-list-add
               (list text-range "" (current-buffer)) file-edit-map))))))))

(defun relocate_docs--handle-filepath (filepath file-edit-map)
  "Move doc-strings out of FILEPATH."
  (let ((buf (find-file filepath)))
    (unless (buffer-file-name)
      (error "Expected a buffer with a file to be set!"))

    (let ((project-root (relocate_docs--root-path-from-filename (file-truename filepath))))
      (unless project-root
        (error "Unable to find the projects root directory"))

      (with-current-buffer buf
        (relocate_docs--handle project-root file-edit-map)))))

;; ---------------------------------------------------------------------------
;; Parse Command Line & Scan Files

(defun relocate_docs--handle-args (args)
  "Handle Command line ARGS.

Return a non-zero exit code if checking failed to run (not if it found errors)."

  (setq relocate_docs-build-dir (relocate_docs--extract-arg-value args "--build-dir" t))

  (unless (file-exists-p relocate_docs-build-dir)
    (relocate_docs--error-and-exit "Error '%s' does not point to a directory!"
                                   relocate_docs-build-dir))

  (let ((path (concat (file-name-as-directory relocate_docs-build-dir) "compile_commands.json")))
    (unless (file-exists-p path)
      (relocate_docs--error-and-exit
       (concat
        "Error '%s' does not exist!\n"
        "Set: 'CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON' in the CMakeCache.txt file.")
       path)))

  (with-demoted-errors "Relocate-docs error: %S"
    (setq relocate_docs-prompt (relocate_docs--extract-arg-exist args "--prompt"))
    (setq relocate_docs-prompt-editor (relocate_docs--extract-arg-value args "--prompt-editor"))

    (setq relocate_docs-exclude-regexp (relocate_docs--extract-arg-value args "--exclude-regexp"))
    (when relocate_docs-exclude-regexp
      (when-let ((err (relocate_docs--regexp-error-check relocate_docs-exclude-regexp)))
        (relocate_docs--error-and-exit
         "Invalid regular expression \"--exclude-regexp=%s\", error: %S"
         relocate_docs-exclude-regexp err))))

  (relocate_docs--package-init)

  ;; Install `lsp' if it's not found.
  (relocate_docs--package-ensure 'lsp-mode)

  (catch 'exit
    (let ((files (list))
          (files-length 0))
      ;; Unlikely, check just in case.
      (unless args
        (relocate_docs--error-and-exit "Warning: no arguments given, expected file paths!")
        (throw 'exit 1))

      (while args
        (let ((filepath (pop args)))
          (cond
           ((file-exists-p filepath)
            (setq filepath (expand-file-name filepath))
            (push filepath files)
            (setq files-length (1+ files-length)))
           (t
            (message "Warning: missing file: %S" filepath)))))

      ;; Pre-load files.
      (let ((index 0))
        (dolist (f files)
          (message "Preload [%d%%]: %S" (truncate (/ (* 100 index) files-length)) f)
          (find-file f)
          (setq index (1+ index))))

      (message "")
      (message "Scanning...")

      ;; FIXME: not nice, didn't find a solution to detect when xref lookups would be available.
      ;; Sigh, is there no way to request LSP has valid info?
      ;; https://emacs.stackexchange.com/questions/69579
      (sleep-for 0.1)

      ;; Hash map `file-edit-map':
      ;; - Key: filename
      ;; - Value: (buf . (edits ...))
      (let ((file-edit-map (make-hash-table :test 'equal)))

        (while files
          (let ((filepath (pop files)))
            (message "File: %S" filepath)
            (relocate_docs--handle-filepath filepath file-edit-map)))

        ;; Report how many edits to make.
        (let ((buffer-count 0)
              (edit-count 0))

          (maphash
           (lambda (key value)
             (progn
               (setq buffer-count (1+ buffer-count))
               (setq edit-count (+ edit-count (length (cdr value))))))
           file-edit-map)

          (message "")
          (message "Applying %d edits to %d buffers..." edit-count buffer-count))

        (relocate_docs--edit-list-apply file-edit-map)
        (message "Completed!")))

    (throw 'exit 0)))

(let ((exit-code (relocate_docs--handle-args command-line-args-left)))
  (kill-emacs exit-code))

(provide 'relocate_docs)

;; Local Variables:
;; mode: emacs-lisp
;; fill-column: 99
;; End:

;;; relocate_docs.el ends here
